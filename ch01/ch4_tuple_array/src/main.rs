fn main() {
    // tuple
    let tup = (0, "hello", 3.4);
    println!("{:?}", tup);
    println!("tup.0 = {}", tup.0);
    println!("tup.1 = {}", tup.1);
    println!("tup.2 = {}", tup.2);

    let mut tup2 = (1, 'c', 5.6);
    println!("tup2 elements {} {} {}", tup2.0, tup2.1, tup2.2);
    tup2.2 = 3.14;
    println!("tup2 elements {} {} {}", tup2.0, tup2.1, tup2.2);

    // ()
    let tup3 = ();
    println!("tup3 {:?}", tup3);
    println!("Array");

    let mut arr = [11, 23, 34];
    arr[0] = 999;
    println!("{}", format!("arr len is {}, first element is {}", arr.len(), arr[0]));

    for elem in arr {
        println!("{}", elem);
    }
    let ar = [5; 3];
    for i in ar {
        println!("{}", i);
    }

    // ownership
    let arr_item = [1, 2, 3, 4, 5];
    let tup_item = (2, "abc");
    println!("arr_item = {:?}", arr_item);
    println!("tup_item = {:?}", tup_item);
// Clone
    let arr_ownership = arr_item;
    let tup_ownership = tup_item;
    println!("arr_ownership = {:?}", arr_ownership);
    println!("tup_ownership = {:?}", tup_ownership);

    let a = 3;
    let b = a;
    println!("a = {a}, b = {b}");
    // copy
    // move ownership
    let string_item = String::from("Hello");
    let _string_item_tt = string_item;
}
