fn main() {
    // 不可变命名
    let _nice_count = 100;//自动推导类型 i32
    let _nice_number: i64 = 54;

    // 可变命名
    let mut _count = 10;
    _count = 12;

    // Shadowing
    let x = 5;
    {
        // 命名空间
        let x = 10;
        println!("inner x = {}", x);
    }// 内部的x变量作用域结束，外部的x变量被覆盖

    println!("outer x = {x}");// 新写法


    let x = "hello";
    println!("new x = {x}");

    // 可以重定义类型 可变性
    let mut _x = "this";
    _x = "that";
    println!("mut x = {_x}");
}
