static MY_STATIC: i32 = 42;
static mut MY_MUT_STATIC: i32 = 42;

fn main() {
    // Const
    const SECOND_HOUR: usize = 3_600;
    const SECOND_DAY: usize = 24 * SECOND_HOUR;// compile-time constant
    println!("Second day: {SECOND_DAY}");

    {
        const SE: usize = 1_000;
        println!("1000 seconds in a second: {SE}")
    }

    println!("My static: {MY_STATIC}");
    unsafe {
        MY_MUT_STATIC = 32;
        println!("My mutable static: {MY_MUT_STATIC}");
    }
}
