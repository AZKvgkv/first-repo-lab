fn move_func(p1: i32, p2: String) {
    println!("p1: {}, p2: {}", p1, p2);
}
// borrow
fn print_value(value: &i32) {
    println!("{value}");
}
fn string_func_borrow(s: &String) {
    println!("原汁原味:{}", s);
    println!("大写:{}", (*s).to_uppercase());
    println!("小写:{}", (*s).to_lowercase());
}
#[derive(Debug)]
struct Point {
    x: i32,
    y: i32,
}
fn modify_point(point: &mut Point) {
    (*point).x += 10;
    point.y += 10;
}
fn main() {
    let n = 12;
    let s = String::from("hello");
    move_func(n, s);
    // n and s are moved here, and cannot be used anymore.
    println!("n is {n}");
    // println!("s is {}", s);

    let s = String::from("halo");
    print_value(&n);
    print_value(&n);
    string_func_borrow(&s);
    println!("s is {}", s);

    let mut p: Point = Point { x: 0, y: 0 };
    println!("p is {:#?}", p); //更详细
    println!("p is {:?}", p);
    modify_point(&mut p);
    println!("p is {:#?}", p);
    println!("p is {:?}", p);
}
