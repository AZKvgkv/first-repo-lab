fn func_copy_back() -> i32 {
    let n: i32 = 99;
    n
}
fn func_non_copy_back() -> String {
    // let s: String = "Hello".to_string();  // 这个方式也是可以的
    let str: String = String::from("hello Rust");
    str
}
fn get_mess(mark:i32)->&'static str{
    if mark >= 90{
        "Good!!! 🥳🎉"
    } else{
        "Bad... 😢"
    }
}
fn main() {
    let i: i32 = func_copy_back();
    println!("The value of i is {}", i);
    let s: String = func_non_copy_back();
    println!("The value of s is {}", s);
    println!("{}", get_mess(i));
}
