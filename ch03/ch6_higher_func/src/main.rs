fn mul_twice(f: fn(i32) -> i32, x: i32) -> i32 {
    f(f(x))
}

fn mul(x: i32) -> i32 {
    x * x
}

fn add(x: i32) -> i32 {
    x + 12
}

fn main() {
    let result = mul_twice(mul, 2);
    println!("Result: {}", result);
    let res: i32 = mul_twice(add, 12);
    println!("Res: {res}");

    // 数学计算
    let numbers = vec![1, 2, 3, 4, 5];
    let res: Vec<_> = numbers.iter().map(|x| x * x).collect(); //自己乘自己
    println!("Res: {:?}", res);
    // filter
    let numbers = vec![1, 2, 3, 4, 5, 6, 7, 8, 10];
    // ref ref_mut move
    let evens = numbers
        .into_iter()
        .filter(|&x| x % 2 == 0)
        .collect::<Vec<_>>(); // 偶数
    println!("Evens: {:?}", evens);

    let numbers = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    let sum = numbers.iter().fold(0, |acc, &x| acc + x); // 累加
    println!("Sum: {}", sum);
}
