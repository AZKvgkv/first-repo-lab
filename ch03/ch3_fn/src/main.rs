fn add(x: i32, y: i32) -> i32 {
    x + y
}

fn change_i32(mut x: i32) {
    x += 4;
    println!("fn x = {x}");
}
fn modify_i32(x: &mut i32) {
    *x += 4;
}
#[derive(Copy, Clone)]
struct Point {
    x: i32,
    y: i32,
}

fn print_point(point: Point) {
    println!("Point = ({},{})", point.x, point.y);
}
fn main() {
    println!("Hello, world!");
    let a = 1;
    let b = 2;
    let c = add(a, b);
    println!("c = {c}");
    let mut x = 10;
    change_i32(x);
    modify_i32(&mut x);
    println!("x = {x}");
    let s = Point { x: 1, y: 2 };
    print_point(s); // 所有权已经消失
    println!("s = ({},{})", s.x, s.y); // 但是数据仍然存在
}
