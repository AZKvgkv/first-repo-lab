use std::num::ParseIntError;

fn find_first_even(numbers: Vec<i32>) -> Option<i32> {
    let first_even = numbers.iter().find(|&n| n % 2 == 0)?;
    println!("Option");
    Some(*first_even)
}

// 传递错误
fn parse_number(input: &str) -> Result<i32, ParseIntError> {
    let val = input.parse::<i32>()?;
    Ok(val)
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let result_ok: Result<i32, &str> = Ok(32);
    let value: i32 = result_ok.unwrap();
    println!("The value is {}", value);

    let result_ok: Result<i32, ParseIntError> = Ok(32);
    let value: i32 = result_ok?;
    println!("The value is {}", value);

    let numbers = vec![1, 2, 3, 4, 5];
    match find_first_even(numbers) {
        Some(first_even) => println!("The first even number is {}", first_even),
        None => println!("No even number found"),
    }

    match parse_number("d") {
        Ok(value) => println!("The value is {}", value),
        Err(error) => println!("Error: {}", error),
    }

    Ok(())
}
