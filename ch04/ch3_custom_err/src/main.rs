#[derive(Debug)]
struct MyeError {
    message: String,
}

impl std::fmt::Display for MyeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "My custom error: {}", self.message)
    }
}

impl std::error::Error for MyeError {
    fn description(&self) -> &str {
        &self.message
    }
    // cause &String => &str
}

fn func() -> Result<(), MyeError> {
    Err(MyeError {
        // message: "Something went wrong".to_string(),
        message: "Something went wrong".to_owned(),
    })
}
fn main() -> Result<(), MyeError> {
    match func() {
        Ok(()) => println!("func ok!"),
        Err(e) => println!("func error: {}", e),
    }
    func()?;
    println!("main ok!, 如果是error, 则这一行语句是不会打印的");
    Ok(())
}
