fn divide(a: i32, b: i32) -> Result<f64, String> {
    if b == 0 {
        return Err(String::from("Error: Cannot divide by zero"));
    }
    Ok(a as f64 / b as f64)
}

fn find_element(array: &[i32], target: i32) -> Option<usize>{
    for (index, element) in array.iter().enumerate() {
        if (*element) == target {
            return Some(index);
        }
    }
    None
}
fn main() {
    // result
    match divide(1, 2) {
        Ok(number) => println!("{number}"),
        Err(err) => println!("{err}"),
    }

    match divide(1, 0){
        Ok(number) => println!("{number}"),
        Err(err) => println!("{err}"),
    }

    // option
    let arr = [1, 2, 3, 4, 5];
    match find_element(&arr, 3) {
        Some(index) => println!("Element found at index {index}"),
        None => println!("Element not found in array"),
    }

    match find_element(&arr, 6) {
        Some(index) => println!("Element found at index {index}"),
        None => println!("Element not found in array"),
    }

    // //panic
    // let vec = vec![1, 2, 3];
    // vec[5]; // this will cause a panic
}
