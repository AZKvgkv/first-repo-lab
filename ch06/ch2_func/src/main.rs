// 交换
fn swap<T>(a: T, b: T) -> (T, T) {
    (b, a)
}

struct Point<T> {
    x: T,
    y: T,
}

impl<T> Point<T> {
    fn new(x: T, y: T) -> Self {
        Point { x, y }
    }
    // 方法
    fn get_coordinates(&self) -> (&T, &T) {
        (&self.x, &self.y)
    }
}

fn main() {
    let result = swap(1, 0);
    println!("{:?}", result);

    let result = swap(1.0, 0.0);
    println!("{:?}", result);

    let str = swap("he", "her");
    println!("{:?}", str);
    println!("{}", str.0);
    println!("{:?}", str.1);

    let str1 = swap(str.0, str.1);
    println!("{:?}", str1);

    let i32_point = Point::new(2, 5);
    let f64_point = Point::new(2.0, 5.0);
    let (x_1, y_1) = i32_point.get_coordinates();
    let (x_2, y_2) = f64_point.get_coordinates();

    println!("i32_point: ({}, {})", x_1, y_1);
    println!("f64_point: ({}, {})", x_2, y_2);

    //String 不要用&str

    let str_point = Point::new("hello", "world");
    println!("str_point: ({}, {})", str_point.x, str_point.y);
}
