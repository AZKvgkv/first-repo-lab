#[derive(Debug)]
struct Point<T> {
    _x: T,
    _y: T,
}

#[derive(Debug)]
struct PointTwo<T, E> {
    _x: T,
    _y: E,
}
fn main() {
    let c1 = Point { _x: 1.0, _y: 2.0 };
    let c2 = PointTwo { _x: 'x', _y: 'y' };
    println!("c1 {:?} and c2 {:?}", c1, c2);
    let c =PointTwo { _x: 1, _y: 'y' };
    println!("c: {:?}", c);
}
