use std::ops::Add;
// 编译时
#[derive(Debug)]
struct Point<T> {
    x: T,
    y: T,
}

// T 这样的类型可以执行相加的操作
impl<T> Add for Point<T>
where
    T: Add<Output = T>,
{
    type Output = Point<T>;
    fn add(self, other: Self) -> Self::Output {
        Point {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}
fn main() {
    println!("Hello, world!");
    let i1: Point<i32> = Point { x: 1, y: 2 };
    let i2: Point<i32> = Point { x: 3, y: 4 };
    let sum: Point<i32> = i1 + i2;
    println!("{:?}", sum);
    let f1: Point<f64> = Point { x: 1.0, y: 2.3 };
    let f2: Point<f64> = Point { x: 1.1, y: 2.0 };
    let sum: Point<f64> = f1 + f2;
    println!("{:?}", sum);
}
