// Debug Clone Copy PartialEq
// 层级
#[derive(Debug, Clone, Copy)]
// #[allow(dead_code)]
enum Race {
    White,
    Yellow,
    Black,
}
impl PartialEq for Race {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Race::White, Race::White) => true,
            (Race::Yellow, Race::Yellow) => true,
            (Race::Black, Race::Black) => true,
            _ => false,
        }
    }
}

#[derive(Debug, Clone)]
struct User {
    id: u32,
    name: String,
    race: Race,
}
impl PartialEq for User {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id && self.name == other.name && self.race == other.race
    }
}

fn main() {
    println!("Hello, world!");
    let user = User {
        id: 1,
        name: "Alice".to_string(),
        race: Race::White,
    };
    println!("user: {:#?}", user);
    let user_1 = User {
        id: 2,
        name: "Chris".to_string(),
        race: Race::Yellow,
    };
    println!("user_1: {:#?}", user_1);
    let usr = User {
        id: 3,
        name: "Bob".to_owned(),
        race: Race::Black,
    };
    println!("usr: {:#?}", usr);
    println!("----clone----");
    let user_clone = user.clone();
    println!("user_clone: {:#?}", user_clone);
    println!("{}", user == user_clone);
}
