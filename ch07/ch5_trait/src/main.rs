use std::collections::VecDeque;
// 多态
trait Driver {
    fn drive(&self, distance: u32);
}
struct Car;
impl Driver for Car {
    fn drive(&self, distance: u32) {
        println!("Driving {} km with a Car", distance);
    }
}

struct SUV;
impl Driver for SUV {
    fn drive(&self, distance: u32) {
        println!("Driving {} km with a SUV", distance);
    }
}

fn road(vehicle: &dyn Driver, distance: u32) {
    vehicle.drive(distance);
}

// 继承思想
// 单向特质
trait Queue {
    fn len(&self) -> usize;
    fn push_back(&mut self, value: i32);
    fn pop_front(&mut self) -> Option<i32>;
}

// 双向特质
trait Deque: Queue {
    fn push_front(&mut self, value: i32);
    fn pop_back(&mut self) -> Option<i32>;
}

#[derive(Debug)]
struct List {
    data: VecDeque<i32>,
}

impl List {
    fn new() -> Self {
        Self {
            data: VecDeque::<i32>::new(),
        }
    }
}

impl Deque for List {
    fn push_front(&mut self, value: i32) {
        self.data.push_front(value)
    }

    fn pop_back(&mut self) -> Option<i32> {
        self.data.pop_back()
    }
}

impl Queue for List {
    fn len(&self) -> usize {
        self.data.len()
    }

    fn push_back(&mut self, value: i32) {
        self.data.push_back(value)
    }

    fn pop_front(&mut self) -> Option<i32> {
        self.data.pop_front()
    }
}

fn main() {
    println!("Hello, world!");
    road(&Car, 100);
    road(&SUV, 200);
    println!("--------------------------");
    let mut list = List::new();
    list.push_back(1);
    list.push_front(12);
    println!("{:?}", list);
    list.push_front(13);
    println!("{:?}", list);
    list.push_back(12);
    println!("{:?}", list);
    println!("{}", list.pop_back().unwrap());
    println!("{:?}", list);
    println!("{}", list.pop_front().unwrap());
    println!("{:?}", list);
    println!("--------------------------");
    list.len();
    println!("{:?}", list);
}
