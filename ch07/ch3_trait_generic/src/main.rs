trait Overview {
    fn overview(&self) -> String {
        String::from("Course")
    }
}

trait Another {
    fn hell(&self) {
        println!("welcome to hell")
    }
}

#[allow(dead_code)]
struct Course {
    headline: String,
    author: String,
}

impl Overview for Course {}
impl Another for Course {}

#[allow(dead_code)]
struct AnotherCourse {
    headline: String,
    author: String,
}

impl Another for AnotherCourse {}

fn call_overview(item: &impl Overview) {
    println!("Overview {}", item.overview());
}

fn call_overview_generic<T: Overview>(item: &T) {
    println!("Overview {}", item.overview());
}

fn call_overview_t(item: &impl Overview, item1: &impl Overview) {
    println!("Overview {} {}", item.overview(), item1.overview());
}

fn call_overview_tt<T: Overview>(item: &T, item1: &T) {
    println!("Overview {} {}", item.overview(), item1.overview());
}

// 多绑定
fn call_mul_bind(item: &(impl Overview + Another)) {
    println!("Overview {}", item.overview());
    item.hell()
}
fn call_mul_bind_generic<T>(item: &T)
where
    T: Overview + Another,
{
    println!("Overview {}", item.overview());
    item.hell();
}

fn main() {
    println!("Hello, world!");
    let c0 = Course {
        headline: String::from("Rust Programming"),
        // author: String::from("John Doe"),
        author: "John Doe".to_owned(),
    };
    let c1 = Course {
        headline: String::from("Zig Programming"),
        author: "Andrew Kelley".to_owned(),
    };
    let c2 = Course {
        headline: String::from("Go Programming"),
        author: "K Thompson".to_owned(),
    };
    call_overview(&c0);
    call_overview_generic(&c1);
    call_overview_t(&c1, &c2);
    println!("--------------------------------");
    call_overview_tt(&c1, &c2);
    call_mul_bind(&c1);
    call_mul_bind_generic(&c1);
}
