trait Greeter {
    fn greet(&self);
    fn hello(){
        println!("Hello, world!");
    }
}

struct Person{
    name: String,
    age: u8,
}

impl Greeter for Person {
    fn greet(&self) {
        println!("Hello, my name is {} and I am {} years old.", self.name, self.age);
    }
}
fn main() {
    let person =Person{
        name:"AZ".to_owned(),
        age:25,
    };
    person.greet();
    Person::hello();
}