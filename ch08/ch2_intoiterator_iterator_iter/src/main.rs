fn main() {
    println!("Hello, world!");
    // vec
    let v = vec![1, 2, 3, 4, 5]; // intoIterator 特质 into_iter() 方法
    let iter = v.into_iter(); // 转化为迭代器
    let sum: i32 = iter.sum(); // 迭代器的 sum() 方法
    println!("sum = {}", sum);
    // println!("{:?}", v) // 不能打印数组,涉及到所有权

    // array
    let array = [1, 2, 3, 4, 5];
    let iter: std::slice::Iter<i32> = array.iter(); // 转化为迭代器
    let sum: i32 = iter.sum(); // 迭代器的 sum() 方法
    println!("sum = {}", sum);
    println!("{:?}", array);

    // char
    let text = "hello world";
    let iter = text.chars(); // 转化为迭代器
    let uppercase = iter.map(|c| c.to_ascii_uppercase()).collect::<String>();
    println!("uppercase = {}", uppercase);
    println!("{:?}", text);
}
