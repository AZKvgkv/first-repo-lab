fn main() {
    let vec = vec![1, 2, 3, 4, 5];
    // iter()
    for &item in vec.iter() {
        println!("{}", item);
    }
    println!("Hello, world!");
    // 可变引用
    let mut vec2 = vec![1, 2, 3, 4, 5];
    for item in vec2.iter_mut() {
        *item += 2;
    }
    println!("{:?}", vec2);
    let vec3 = vec![1, 2, 3, 4, 5];
    for item in vec3.into_iter() {
        println!("{}", item);
    }
    // println!("{:?}", vec3); // 所有权转移 move
}
