#[derive(Debug)]
struct Stack<T> {
    data: Vec<T>,
}

impl<T> Stack<T> {
    fn new() -> Self {
        Stack { data: Vec::new() }
    }
    // 入栈
    fn push(&mut self, item: T) {
        self.data.push(item);
    }
    // 出栈
    fn pop(&mut self) -> Option<T> {
        self.data.pop()
    }

    // 不可变引用
    fn iter(&self) -> std::slice::Iter<T> {
        self.data.iter()
    }
    fn iter_mut(&mut self) -> std::slice::IterMut<T> {
        self.data.iter_mut()
    }
    fn into_iter(self) -> std::vec::IntoIter<T> {
        self.data.into_iter()
    }
}

fn main() {
    println!("Hello, world!");
    let mut my_stack = Stack::new();
    my_stack.push(1);
    my_stack.push(2);
    my_stack.push(3);

    for item in my_stack.iter() {
        println!("Item {}", item);
    }
    println!("{:?}", my_stack);

    for item in my_stack.iter_mut() {
        *item *= 2;
    }
    println!("{:?}", my_stack);

    for item in my_stack.into_iter() {
        println!("Item {}", item);
    }

    println!("------");
    let mut stack: Stack<i32> = Stack::new();
    let result: Option<_> = stack.pop();
    println!("{:?}", result);
}
