enum Color {
    Red,
    _Green,
    _Blue,
    _Yellow,
    _Purple,
}

fn print_color(my_color: Color) {
    match my_color {
        Color::Red => println!("Red"),
        Color::_Green => println!("Green"),
        Color::_Blue => println!("Blue"),
        Color::_Yellow => println!("Yellow"),
        Color::_Purple => println!("Purple"),
    }
}

enum BuildingLocation {
    Number(i32),
    Name(String), // 不要使用 &str
    Unknown,
}

impl BuildingLocation {
    fn print_location(&self) {
        match self {
            BuildingLocation::Number(c) => println!("building Number: {c}"),
            BuildingLocation::Name(s) => println!("building Name: {}", *s),
            BuildingLocation::Unknown => println!("building Unknown"),
        }
    }
}
fn main() {
    let a = Color::Red;
    print_color(a);

    let _house = BuildingLocation::Name("my_house".to_string());
    let _house = BuildingLocation::Number(123);
    let house = BuildingLocation::Unknown;

    house.print_location();
}
