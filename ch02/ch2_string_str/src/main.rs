// struct Person <'a>{
//     name: &'a str,
//     color: String,
//     age: i32
// }
// &String &str
fn print(data:&str){
    println!("{}",data);
}

// only &String
fn print_string_borrow(data:&String){
    println!("{}",data);
}
fn main() {
    // String &str
    let name = String::from("Value Cpp");
    // String::from
    // to_string()
    // to_owned()
    let course = "Rust".to_string();
    let new_name = name.replace("C++", "CPP");
    println!("name: {name}");
    println!("course: {course}");
    println!("new_name: {new_name}");
    let rust = "\x52\x75\x73\x74"; // ascii code for RUST
    println!("rust: {rust}");

    // String &str
    // let color = "green".to_string();
    // let name = "Bob";
    // let people = Person{
    //     name: name,
    //     color: color,
    //     age: 25
    // };

    // func
    let value = "value".to_owned();
    print(&value);
    print("data");
    print_string_borrow(&value);
}
