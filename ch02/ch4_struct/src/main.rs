enum Flavor {
    Spicy,
    Sweet,
    _Fruity,
}
struct Drink {
    flavor: Flavor,
    price: f64,
}

impl Drink {
    // 关联变量
    const MAX_PRICE: f64 = 100.0;
    // 方法
    fn buy(&self) {
        if self.price > Drink::MAX_PRICE {
            println!("Sorry, the price is too high! I'm poor");
        } else {
            println!("I will buy it!");
        }
    }
    // 关联函数
    fn new(price: f64) -> Self {
        Drink {
            flavor: Flavor::Spicy,
            price,
        }
    }
}

fn print_drink(drink: Drink) {
    match drink.flavor {
        Flavor::Spicy => println!("Spicy drink"),
        Flavor::Sweet => println!("Sweet drink"),
        Flavor::_Fruity => println!("Fruity drink"),
    }
    println!("Price: {}", drink.price)
}
fn main() {
    let sweet = Drink {
        flavor: Flavor::Sweet,
        price: 80.0,
    };
    println!("Sweet drink's price: {}", sweet.price);
    print_drink(sweet);
    let spicy = Drink::new(120.0);
    spicy.buy();
}
