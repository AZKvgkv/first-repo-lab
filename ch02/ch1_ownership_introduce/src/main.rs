fn get_length(s: String) -> usize {
    println!("String: {}", s);
    // 函数结束之后 main::s1 也销毁了
    s.len()
}
fn main() {
    // copy move
    //copy
    let c1 = 1;
    let c2 = c1;
    println!("c1: {}, c2: {}", c1, c2);


    let s1 = String::from("the value");
    let s2 = s1.clone();// s1 的所有权转移给 s2
    println!("s1: {}, s2: {}", s1, s2);


    let len = get_length(s1);
    println!("len: {len}");
    

    let back = first_word("hello world");
    println!("back: {back}");
    let back = first_word("we are the world");
    println!("back: {back}");
}


fn _dangle() -> String {
    "hello".to_owned()
}

// 静态的生命周期
fn _dangle_static() -> &'static str {
    "jdk21"
}

// String 与 &str vec u8 ref
fn first_word(s: &str) -> &str {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' '{
            return &s[0..i];
        }
    }
    &s[..]
}